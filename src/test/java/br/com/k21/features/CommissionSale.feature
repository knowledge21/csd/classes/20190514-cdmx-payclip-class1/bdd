Feature: Commission Sales

  Scenario: A $500 sale should result in a commission of 25
    Given a sale of 500
    When The commission is calculated
    Then we should get 25

   Scenario: A $10001 sale should result in a commission of 600.06
    Given a sale of 10001
    When The commission is calculated
    Then we should get 600.06
