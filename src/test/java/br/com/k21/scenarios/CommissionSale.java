package br.com.k21.scenarios;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class CommissionSale {

    private WebDriver driver;

    double x;

    @Given("^a sale of (\\d+)$")
    public void given_a_sale_of_x(double x) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        System.setProperty("webdriver.chrome.driver", "/home/flaviocpontes/chromedriver");
        driver = new ChromeDriver();
        this.x = x;
    }

    //^I enter "([^"]*)" movie name$
    @When("^The commission is calculated")
    public void The_commission_is_calculated() throws Throwable {


        driver.navigate().to("http://localhost:8080/"+ x);


    }

    @Then("^we should get (\\d+)$")
    public void we_should_get_y(double y) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        Assert.assertEquals(y,Double.parseDouble( driver.findElement(By.tagName("body")).getText()),0);
    }

    @Given("^a sale of \\$10001$")
    public void given_a_sale_of_10001() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        System.setProperty("webdriver.chrome.driver", "/home/flaviocpontes/chromedriver");
        driver = new ChromeDriver();

    }

    //^I enter "([^"]*)" movie name$
    @When("^The commission is calculated as 6% of the sale")
    public void the_commission_is_calculated_as_6_of_the_sale() throws Throwable {


        driver.navigate().to("http://localhost:8080/10001");


    }

    @Then("^we should get 600.06")
    public void we_should_get_600_06() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        Assert.assertEquals("600.06",driver.findElement(By.tagName("body")).getText());
    }
}
